C Reads and write very simple coordinates files. The first three fields in each
C line must be the x, y and z component, respectively.
C
C Author: Moritz R. Schaefer
      module mod_xyz
C
      implicit double precision (a - h, o - z)
      integer, private, parameter :: FID = 1 ! Fileunit
C
      contains
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C Returns the number of lines in file with name filename
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      integer function get_lines(filename)
      character(len = *), intent(in) :: filename
C
      open(
     $    unit   = FID,
     $    file   = filename, 
     $    status = 'old',
     $    access = 'sequential',
     $    form   = 'formatted',
     $    action = 'read'
     $)
C
      get_lines = 0
      do 10
          read (1, *, iostat = io)
          if (io /= 0) exit
          get_lines = get_lines + 1
10    continue
      close(FID)
      end function get_lines
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C Read in xyz file with name `filename`. Returns content as an double
C precision array
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      function read_xyz(nline, filename) result(xyz)
      character(len = *), intent(in) :: filename
      integer, intent(in) :: nline
      dimension xyz(3, nline)
C
      open(
     $    unit   = FID,
     $    file   = filename, 
     $    status = 'old',
     $    access = 'sequential',
     $    form   = 'formatted',
     $    action = 'read'
     $)
C
      do 20, iline = 1, nline
          read (FID, *) xyz(:, iline)
20    continue
      close(FID)
C
      end function read_xyz
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C Formatted dump of an array xyz(3, *) to a file with name `filename`.
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine write_xyz(nline, xyz, filename)
        character(len = *), intent(in) :: filename
        integer, intent(in) :: nline ! number of lines to write
        dimension xyz(3, nline)
C
        open(
     $      unit   = FID,
     $      file   = filename, 
     $      status = 'new',
     $      access = 'sequential',
     $      form   = 'formatted',
     $      action = 'write'
     $  )
C
        do 30, iline = 1, nline
            write(FID, *) xyz(:, iline)
30      continue
        close(FID)
C
      end subroutine write_xyz
C
      end module mod_xyz
