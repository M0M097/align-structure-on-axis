#!/bin/sh
input=${5-benzene.xyz}
output=${6-new_benzene.xyz}
# Strip header and atomsymbols
./xyz2input.awk $input > tmp_input
# Perform transformation
../align_on_z_axis ${1-1} ${2-5} ${3-n} ${4-0} tmp_input tmp_output 
# Add header and atomsymbols
./output2xyz.awk $input tmp_output > $output 
# Remove intermediary files
rm tmp_*
