#!/bin/awk -f
FNR == 1 {file++} # Count files
file == 1 && FNR <= 2 {print $0} # Print header
file == 1 && FNR >= 3 {atsymb[++nat] = $1} # Store atomsymbol
file == 2 {print atsymb[FNR] $0} # Print symbol before transformed coordinates
