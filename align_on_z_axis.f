      program align_on_z_axis
      use mod_xyz, only: read_xyz, write_xyz, get_lines
      implicit double precision (a - h, o - z)
C 
      character(len = *), parameter :: HELPTEXT(*) = (/
     $'Rotate points such that the z-axis penetrates two given points.',
     $'Author: Moritz R. Schaefer moritz.schaefer-f91@rub.de          ',
     $'                                                               ',
     $'Reads xyz coordinates from `filename`, translates and rotates  ',
     $'coordinates such that point 1 has coordinates (0, 0,`final z   ',
     $'position`) and point 2 has coordinates (0, 0,z).               ',
     $'                                                               ',
     $'The structure can also be translated such that all points are  ',
     $'within the smallest axis-aligned box (only useful for periodic ',
     $'systems)                                                       ',
     $'                                                               ',
     $'All internal distances and angles are preserved.               ',
     $'                                                               ',
     $'Requires 5 plus one optional argument. Usage:                  ',
     $'1) line number containing xyz positions of point 1             ',
     $'2) line number containing xyz positions of point 2             ',
     $'3) if `y`, translate all points into a axis-aligned box        ',
     $'4) final z position of point 1, ignored if 3) is `y`           ',
     $'5) filename, must contain one xyz triplet per line             ',
     $'6) output filename, if not given new_`filename` will be used   '
     $/)
C
      dimension p1(3), p2(3), xyz_min(3)
      dimension rotmat_z(3, 3), rotmat_y(3, 3), rotmat(3, 3)
      allocatable xyz(:, :)
C
      character(len = 200) :: output_filename, filename, arg
      character translate_to_box
      integer, parameter :: NHELP = size(HELPTEXT)
C
C Print help for incorrect command line args
      iargc = command_argument_count()
      if (iargc /= 5 .and. iargc /= 6) then
          do 10, ihelp = 1, NHELP
               write(*, *) HELPTEXT(ihelp)
10        continue
          stop 1
      end if
C
C Parse command line args
      call get_command_argument(1, arg)
      read (arg, *) iat1
      call get_command_argument(2, arg)
      read (arg, *) iat2
      call get_command_argument(3, arg)
      read (arg, *) translate_to_box
      call get_command_argument(4, arg)
      read (arg, *) offset
      call get_command_argument(5, filename)
      if (iargc == 6) then
          call get_command_argument(6, output_filename)
      else
          output_filename = "new_" // filename
      end if
C 
C Read input file
      nline = get_lines(filename)
      allocate(xyz(3, nline))
      xyz = read_xyz(nline, filename)
C 
C Error handling
      if (iat1 .gt. nline .or. iat2 .gt. nline) then
          write(*, *) "atom 1 or 2 not inside structure"
          stop 1
      end if
C
C Translate so that p1 is at the orign
      p1 = xyz(:, iat1)
      do 20, iline = 1, nline
          xyz(:, iline) = xyz(:, iline) - p1
20    continue
C 
C Given a point P(a, b, c), we want to align it with the z-axis by 
C performing a rotation.
      p2 = xyz(:, iat2)
C 
C Rotation around z
C Once we've rotated the point by an angle -θ around the z-axis, 
C it will lie on the x-y plane.
      theta = atan2(p2(2), p2(1))
  
C The rotation matrix for rotation around the z-axis by an angle θ is:
      rotmat_z = transpose(reshape((/
     $    cos(-theta), -sin(-theta), 0. D0,
     $    sin(-theta),  cos(-theta), 0. D0,
     $        0. D0  ,   0. D0     , 1. D0
     $/), (/3, 3/)))
  
C Rotation around y
C To align it with the z-axis completely, we need to rotate it around the
C y-axis. The y-component of the rotated point in the x-y plane becomes 
C the new z-component after the second rotation. Let's denote this 
C y-component as y′. Using trigonometry, we can find y′ as:
      y_prime = sqrt(p2(1)**2 + p2(2)**2)
C 
C Then, the angle φ can be calculated as φ = atan2(y′,c). This angle φ
C will be the angle by which you rotate around the y-axis to bring the
C point (a, b, c) onto the z-axis.
      psi = atan2(y_prime, p2(3))
  
C The rotation matrix for rotation around the y-axis by an angle φ is:
      rotmat_y = transpose(reshape((/
     $     cos(-psi),  0. D0, sin(-psi),
     $        0. D0 ,  1. D0,  0. D0  ,
     $    -sin(-psi),  0. D0, cos(-psi)
     $/), (/3, 3/)))
  
C Thus, the composite rotation matrix to transform the point (a, b, c) to
C have x and y coordinates of 0 while preserving the distance from the 
C origin is:
      rotmat = matmul(rotmat_y, rotmat_z)
  
C Let's use it to rotate xyz
      xyz = matmul(rotmat, xyz)
  
C Translate to the original box or to the desired z position
      if (translate_to_box .eq. 'y') then
          xyz_min = minval(xyz, 2)
          do 30, iline = 1, nline
              xyz(:, iline) = xyz(:, iline) - xyz_min
30        continue
      else
          xyz(3, :) = xyz(3, :) + offset
      end if
C 
C Write output
      call write_xyz(nline, xyz, output_filename)
C
      end program align_on_z_axis
